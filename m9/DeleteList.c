#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 
  
void DeleteList(struct List** head_ref) 
{ 
   struct List* current = *head_ref; 
   struct List* next; 
   while (current != NULL)  
   { 
       next = current->next; 
       free(current); 
       current = next; 
   } 
   *head_ref = NULL; 
} 
  
void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* head = NULL; 
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1);  
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
     
    DeleteList(&head);   
     
    printf("\n Linked list deleted"); 
} 